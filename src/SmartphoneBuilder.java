public interface SmartphoneBuilder {
    // set all objects required to build Smartphone
   public  SmartphoneBuilder buildBaterie(Batterie batterie);
   public  SmartphoneBuilder buildCamera(Camera camera);
   public  SmartphoneBuilder buildConnecteurCharcge(ConnecteurCharge connecteurCharge);
   public  SmartphoneBuilder buildCoqueArriere(CoqueArriere coqueArriere);
   public  SmartphoneBuilder buildEcran(Ecran ecran);
   public Smartphone getSmartphone();
}
