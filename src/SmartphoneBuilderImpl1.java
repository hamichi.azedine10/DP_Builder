public class SmartphoneBuilderImpl1 implements SmartphoneBuilder  {
    private  Batterie batterie;
    private  Camera camera;
    private ConnecteurCharge connecteurCharge;
    private  CoqueArriere coqueArriere;
    private Ecran ecran;
    @Override
    public SmartphoneBuilder buildBaterie(Batterie batterie) {
       this.batterie= batterie;
       return this;
    }

    @Override
    public SmartphoneBuilder buildCamera(Camera camera) {
        this.camera= camera;
        return this;
    }

    @Override
    public SmartphoneBuilder buildConnecteurCharcge(ConnecteurCharge connecteurCharge) {
        this.connecteurCharge = connecteurCharge;
        return this;
    }

    @Override
    public SmartphoneBuilder buildCoqueArriere(CoqueArriere coqueArriere) {
       this.coqueArriere = coqueArriere;
       return this;
    }

    @Override
    public SmartphoneBuilder buildEcran(Ecran ecran) {
        this.ecran= ecran;
        return this;
    }

    @Override
    public Smartphone getSmartphone() {
        return new Smartphone(batterie,camera,connecteurCharge,coqueArriere,ecran);
    }
}
