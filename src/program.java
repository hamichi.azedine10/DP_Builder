import sun.awt.X11.XSystemTrayPeer;

public class program {
    public static void main(String[] args) {
        // DP name : Builder - Category : creation  - scope : object (Composition)
        //  The intent of the Builder design pattern is to:
        //"Separate the construction of a complex object from its representation so that
        // the same construction process can create different representations." [GoF]
        //The Builder design pattern solves problems like:
        //How can a class (the same construction process) create different representations of a complex object?
        // The Builder design pattern provides a solution:
        //Encapsulate creating and assembling the parts of a complex object in a separate Builder object.
        //A class delegates object creation to a Builder object instead of instantiating concrete classes directly.
        // more information see : http://www.w3sdesign.com/GoF_Design_Patterns_Reference0100.pdf


        // dans cette example on a une seule et unique implimetation SmartphoneBuilde :
        SmartphoneBuilder smartphoneBuilder = new SmartphoneBuilderImpl1();

        // construire l'objet  dans le temps
        Smartphone smartphone =  smartphoneBuilder.buildBaterie(new Batterie()).buildCamera(new Camera()).getSmartphone();
        System.out.println(smartphone);
        System.out.println("ajouter la connecteur de charge au telephone");
        smartphone = smartphoneBuilder.buildConnecteurCharcge(new ConnecteurCharge()).getSmartphone();
        System.out.println(smartphone);


            }
}
