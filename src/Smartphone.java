import java.util.ArrayList;
import java.util.List;

public class Smartphone {
    private  Batterie batterie;
    private  Camera camera;
    private ConnecteurCharge connecteurCharge;
    private  CoqueArriere coqueArriere;
    private  Ecran ecran;

    public Smartphone(Batterie batterie, Camera camera, ConnecteurCharge connecteurCharge, CoqueArriere coqueArriere, Ecran ecran) {
        this.batterie = batterie;
        this.camera = camera;
        this.connecteurCharge = connecteurCharge;
        this.coqueArriere = coqueArriere;
        this.ecran = ecran;
    }

    @Override
    public String toString() {
        return "Smartphone{" +
                "batterie=" + batterie +
                ", camera=" + camera +
                ", connecteurCharge=" + connecteurCharge +
                ", coqueArriere=" + coqueArriere +
                ", ecran=" + ecran +
                '}';
    }
}
